﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class BGManager : MonoBehaviour
{

    public GameObject bgTree;
    public Text score_text;
    public GameObject UI_GameOver;

    private bool bGameOver = false;

    private float timer_global = 0.0f;
    private float timer_spawn = 0.0f;
    public float spawn_span = 0.0f;

    // Start is called before the first frame update
    void Start()
    {
        UI_GameOver.SetActive(false);
        
    }

    // Update is called once per frame
    void Update()
    {
        if (bGameOver)
        {
            return;
        }

        // Update timer
        timer_global += Time.deltaTime;
        timer_spawn += Time.deltaTime;

        if (timer_spawn > spawn_span)
        {
            timer_spawn = 0.0f;
            float y = Random.Range(-3.0f, 3.0f);
            Instantiate(bgTree, new Vector3(3, y, 0), Quaternion.identity);

            // set next spawn spawn
            spawn_span = Random.Range(1.0f, 4.0f);
        }


        // Update Score
        score_text.text = "Score: " + timer_global.ToString();
        
    }

    public void GameOver()
    {
        bGameOver = true;
        UI_GameOver.SetActive(true);
    }

    public void Retry()
    {
        SceneManager.LoadScene("main");
    }
}
