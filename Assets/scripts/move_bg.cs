﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class move_bg : MonoBehaviour
{
    public float speed = 1.0f;

    // Game Mode
    BGManager gamemode;

    // Start is called before the first frame update
    void Start()
    {
        gamemode = GameObject.Find("GameMode").GetComponent<BGManager>();

    }

    // Update is called once per frame
    void Update()
    {
        // Move Tree
        transform.position += transform.right * -1 * speed * Time.deltaTime;

        
    }

    // カメラから映らなくなったときによばれる
    void OnBecameInvisible (){
        // 自身を削除する関数
        GameObject.Destroy(this.gameObject);
    }

    void OnTriggerEnter2D(Collider2D t)
    {
        Debug.Log("foo");
        Debug.Log(t.gameObject.name);
        gamemode.GameOver();
    }
}
