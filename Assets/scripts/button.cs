﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class button : MonoBehaviour
{
    // Game Mode
    BGManager gamemode;

    void Start()
    {
        gamemode = GameObject.Find("GameMode").GetComponent<BGManager>();
    }

    public void OnClick()
    {
        gamemode.Retry();
    }
}
